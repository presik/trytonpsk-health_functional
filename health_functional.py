# -*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['FunctionalAntecedentCategory', 'PatientData', 'TimelineOccurrences',
    'FunctionalTimelineReport', 'PatientEvaluation', 'FunctionalSupplement',
    'PatientEvaluationFunctionalReport'
]


class FunctionalAntecedentCategory(ModelSQL, ModelView):
    'Functional Antecedent Type'
    __name__ = 'gnuhealth.functional.antecedent_category'
    name = fields.Char('Name', required=True)


class PatientData:
    __metaclass__ = PoolMeta
    __name__ = 'gnuhealth.patient'
    antecedents = fields.Text('Antecedents')
    mediators = fields.Text('Mediators / Perpetuators',
        depends=['occurrences'])
    triggers = fields.Text('Triggers or Triggering Events',
        depends=['occurrences'])
    signs_symptoms = fields.Text('Signs Symptoms or Diseases Reported',
        depends=['occurrences'])
    occurrences = fields.One2Many('gnuhealth.functional_timeline.occurrences',
        'timeline', 'Ocurrences')

    @fields.depends('mediators', 'triggers', 'sign_symptom', 'occurrences')
    def on_change_occurrences(self):
        mediators = []
        triggers = []
        signs_symptoms = []
        if self.occurrences:
            for oc in self.occurrences:
                if oc.mediator:
                    mediators.append(oc.comment)
                if oc.trigger:
                    triggers.append(oc.comment)
                if oc.sign_symptom:
                    signs_symptoms.append(oc.comment)

        self.mediators = '\n'.join(mediators)
        self.triggers = '\n'.join(triggers)
        self.signs_symptoms = '\n'.join(signs_symptoms)


class FunctionalSupplement(ModelSQL, ModelView):
    'Functional Supplement'
    __name__ = 'gnuhealth.functional.supplement'
    evaluation = fields.Many2One('gnuhealth.patient.evaluation', 'Evaluation',
        required=True)
    name = fields.Char('Name', required=True)
    fast = fields.Char('Fast')
    breakfast = fields.Char('Breakfast')
    middle_morning = fields.Char('Middle Morning')
    lunch = fields.Char('Lunch')
    middle_afternoon = fields.Char('Middle Afternoon')
    dinner = fields.Char('Dinner')
    before_bed = fields.Char('Before Bed')


class PatientEvaluation:
    __metaclass__ = PoolMeta
    __name__ = 'gnuhealth.patient.evaluation'

    STATES = {'readonly': Eval('state') == 'signed'}

    sleep_relaxation = fields.Text('Sleep & Relaxation', states=STATES)
    exercise_move = fields.Text('Exercise & Move', states=STATES)
    nutrition_hidratation = fields.Text('Nutrition & Hidratation', states=STATES)
    stress_adjustment = fields.Text('Stress and Adjustment', states=STATES)
    relations = fields.Text('Relations and Social Network', states=STATES)
    other_functional_prescription = fields.Text('Other Functional Prescription',
        states=STATES)
    exams_lab = fields.Text('Exams Lab', states=STATES)
    supplements = fields.One2Many('gnuhealth.functional.supplement',
        'evaluation', 'Supplements')

    antecedents = fields.Function(fields.Text('Antecedents',
        depends=['patient']), 'on_change_with_antecedents')
    mediators = fields.Function(fields.Text('Mediators / Perpetuators',
        depends=['patient']), 'on_change_with_mediators')
    triggers = fields.Function(fields.Text('Triggers or Triggering Events',
        depends=['patient']), 'on_change_with_triggers')
    signs_symptoms = fields.Function(fields.Text('Signs Symptoms or Diseases Reported',
        depends=['patient']), 'on_change_with_signs_symptoms')
    occurrences = fields.Function(fields.One2Many('gnuhealth.functional_timeline.occurrences',
        'timeline', 'Occurrences'), 'get_patient_data')

    @classmethod
    def __setup__(cls):
        super(PatientEvaluation, cls).__setup__()
        cls._buttons.update({
            'update_timeline': {
                'invisible': Eval('state') == 'draft',
            },
        })

    @fields.depends('patient')
    def on_change_with_antecedents(self, name=None):
        return self.patient.antecedents

    @fields.depends('patient')
    def on_change_with_mediators(self, name=None):
        return self.patient.mediators

    @fields.depends('patient')
    def on_change_with_triggers(self, name=None):
        return self.patient.triggers

    @fields.depends('patient')
    def on_change_with_signs_symptoms(self, name=None):
        return self.patient.signs_symptoms

    def get_patient_data(self, name):
        if name == 'antecedents':
            res = getattr(self.patient, name)
            return res

    @classmethod
    @ModelView.button_action('health_functional.act_update_timeline')
    def update_timeline(cls, records):
        pass


class TimelineOccurrences(ModelSQL, ModelView):
    'Functional Timeline Occurrences'
    __name__ = 'gnuhealth.functional_timeline.occurrences'
    _order_name = 'age'
    timeline = fields.Many2One('gnuhealth.patient',
        'Patient', required=True)
    category = fields.Many2One('gnuhealth.functional.antecedent_category',
        'Antecedent Category', required=True)
    age = fields.Integer('Age')
    comment = fields.Char('Comment', required=True)
    moment = fields.Selection([
        ('preconception', '1. Preconception'),
        ('prenatal', '2. Prenatal'),
        ('birth', '3. Birth'),
        ('infancy_adolescence', '4. Infancy & Adolescence'),
        ('adulthood', '5. Adulthood'),
        ('current_concerns', '6. Current Concerns'),
    ], 'Moment', required=True)
    mediator = fields.Boolean('Mediator')
    trigger = fields.Boolean('Trigger')
    sign_symptom = fields.Boolean('Sign & Symptom')

    @classmethod
    def __setup__(cls):
        super(TimelineOccurrences, cls).__setup__()
        cls._order[0] = ('age', 'ASC')


class FunctionalTimelineReport(Report):
    __name__ = 'gnuhealth.functional_timeline.report'


class PatientEvaluationFunctionalReport(Report):
    __name__ = 'gnuhealth.patient.evaluation'
