# This file is part of Tryton.  The COPYRIGHT file at the top level of this
#repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from .health_functional import (FunctionalAntecedentCategory,
    PatientData, TimelineOccurrences, FunctionalTimelineReport,
    PatientEvaluation, PatientEvaluationFunctionalReport, FunctionalSupplement)
from .survey import (SurveyTemplate, SurveyTemplateLine, Survey, SurveyLine,
    SurveyGroup)

def register():
    Pool.register(
        FunctionalAntecedentCategory,
        PatientData,
        TimelineOccurrences,
        PatientEvaluation,
        Survey,
        SurveyTemplate,
        SurveyTemplateLine,
        SurveyLine,
        SurveyGroup,
        FunctionalSupplement,
        module='health_functional', type_='model')
    Pool.register(
        #SurveyReport,
        PatientEvaluationFunctionalReport,
        FunctionalTimelineReport,
        module='health_functional', type_='report')
