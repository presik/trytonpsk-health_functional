# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime
from trytond.report import Report
from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.pyson import Eval, If, In, Get
from trytond.pool import Pool
from trytond.transaction import Transaction

__all__ = ['SurveyTemplate', 'SurveyTemplateLine',
    'Survey', 'SurveyLine', 'SurveyGroup'] #, 'SurveyReport']


STATES = {'readonly': (Eval('state') != 'draft')}

RESPONSE = {
    'numeric_1_5': [(str(n+1), str(n+1)) for n in range(5)],
    'yes_no': [
        ('yes', 'Yes'),
        ('no', 'No'),
        ('not_apply', 'Not Apply'),
    ],
}


class SurveyGroup(ModelSQL, ModelView):
    "Survey Group"
    __name__ = "health_functional.survey.group"
    name = fields.Char('Name', required=True, select=True)
    code = fields.Char('Code', select=True)
    description = fields.Text('Description', select=True)
    convention = fields.Text('Convention', select=True)
    compute_method = fields.Selection([
        ('sum', 'Sum'),
        ('average', 'Average'),
        ('', ''),
    ], 'Compute Method', select=True)


class SurveyTemplate(ModelSQL, ModelView):
    "Survey Template"
    __name__ = "health_functional.survey_template"
    name = fields.Char('Name', required=True, select=True)
    code = fields.Char('Code', required=True, select=True)
    active = fields.Boolean('Active')
    lines = fields.One2Many('health_functional.survey_template.line',
        'template', 'Lines')


class SurveyTemplateLine(ModelSQL, ModelView):
    "Survey Template Line"
    __name__ = "health_functional.survey_template.line"
    _rec_name = 'ask'
    template = fields.Many2One('health_functional.survey_template',
        'Template Survey', required=True)
    group = fields.Many2One('health_functional.survey.group',
        'Survey Group', required=False)
    sequence = fields.Integer('Sequence', required=True, select=True)
    ask = fields.Char('Ask', required=True, select=True)
    type_response = fields.Selection([
        ('numeric_1_5', 'Numeric 1-5'),
        ('free_text', 'Free Text'),
        ('yes_no', 'Yes or No'),
        ('boolean', 'Boolean'),
    ], 'Type Response', required=True, select=True)

    @staticmethod
    def default_type_response():
        return 'yes_no'


class Survey(Workflow, ModelSQL, ModelView):
    "Survey"
    __name__ = "health_functional.survey"
    number = fields.Char('Number', readonly=True, select=True)
    party = fields.Many2One('party.party', 'Party', states=STATES)
    operator = fields.Many2One('party.party', 'Operator', states=STATES)
    date_time = fields.DateTime('Date', required=True, states=STATES)
    template = fields.Many2One('health_functional.survey_template',
        'Survey Template', required=True, states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[('id', If(In('company',
        Eval('context', {})), '=', '!='), Get(Eval('context', {}),
        'company', 0)), ], states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('process', 'Process'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    lines = fields.One2Many('health_functional.survey.line', 'survey',
        'Lines', states=STATES)
    notes = fields.Text('Notes', states=STATES)

    @classmethod
    def __setup__(cls):
        super(Survey, cls).__setup__()
        cls._order.insert(0, ('number', 'ASC'))
        cls._transitions |= set((
            ('draft', 'process'),
            ('process', 'done'),
            ('process', 'cancelled'),
            ('cancelled', 'draft'),
            ('process', 'draft'),
            ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'done']),
                },
            'cancel': {
                'invisible': Eval('state') != 'process',
                },
            'done': {
                'invisible': Eval('state') != 'process',
                },
            'process': {
                'invisible': Eval('state') != 'draft',
                },
            })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date_time():
        return datetime.now()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('process')
    def process(cls, records):
        cls.set_number(records)

    @fields.depends('template', 'lines')
    def on_change_template(self):
        lines_to_add = []
        if self.template:
            sequence = 1
            groups = []
            for tline in self.template.lines:
                if tline.group and tline.group.id not in groups:
                    groups.append(tline.group.id)
                    lines_to_add.append({
                        'sequence': tline.group.code,
                        'description': tline.group.name,
                        'line_type': 'topic',
                        'comments': tline.group.convention,
                    })
                lines_to_add.append({
                    'sequence': str(sequence),
                    'description': tline.ask,
                    'line_ask': tline.id,
                    'line_type': 'ask',
                })
                sequence += 1
        self.lines = lines_to_add

    @classmethod
    def set_number(cls, records):
        '''
        Fill the number field with the survey sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('health_functional.configuration')
        config = Config(1)

        for record in records:
            if record.number:
                continue
            if not config.health_functional_survey_sequence:
                continue
            number = Sequence.get_id(config.health_functional_survey_sequence.id)
            cls.write([record], {'number': number})


class SurveyLine(ModelSQL, ModelView):
    "Survey Line"
    __name__ = "health_functional.survey.line"
    survey = fields.Many2One('health_functional.survey',
        'Survey', required=True)
    sequence = fields.Char('Sequence')
    description = fields.Char('Description',
        depends=['line_ask', 'line_type'],
        states={'readonly': True}
        )
    line_ask = fields.Many2One('health_functional.survey_template.line',
        'Survey Line')
    response = fields.Selection('selection_response', 'Response',
        depends=['line_ask', 'line_type'], states={
            'invisible': Eval('line_type') != 'ask',
        })
    line_type = fields.Selection([
        ('ask', 'Ask'),
        ('topic', 'Topic')
        ], 'Line Type', states={
            'readonly': True,
        })
    result = fields.Float('Result', digits=(16, 2),
        states={
            'invisible': Eval('line_type') != 'topic',
        })
    comments = fields.Text('Comments')
    group = fields.Many2One('health_functional.survey.group',
        'Survey Group', states={
            'readonly': True
        })

    @classmethod
    def __setup__(cls):
        super(SurveyLine, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))

    @staticmethod
    def default_line_type():
        return 'ask'

    @fields.depends('line_ask', 'response')
    def selection_response(self):
        res = [('', '')]
        if self.line_ask and self.line_ask.type_response in ('numeric_1_5', 'yes_no'):
            res = RESPONSE.get(self.line_ask.type_response)
        return res

    @fields.depends('line_ask', 'group', 'line_type')
    def on_change_with_line_ask(self):
        if self.line_type == 'ask' and self.line_ask and self.line_ask.group:
            if self.line_ask.type_response == 'yes_no':
                return 'yes'
            elif self.line_ask.type_response == 'numeric_1_5':
                return '5'

    @fields.depends('line_ask', 'line_type')
    def on_change_with_description(self, name=None):
        if self.line_type == 'topic':
            if self.line_ask.group:
                return self.line_ask.group.name
        else:
            return self.line_ask.ask

    @fields.depends('line_ask', 'group')
    def on_change_with_group(self):
        if self.line_ask and self.line_ask.group:
            return self.line_ask.group.id


# class SurveyReport(Report):
#     "Survey Report"
#     __name__ = "health_functional.survey"
